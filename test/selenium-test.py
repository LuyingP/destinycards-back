# Generated by Selenium IDE
import pytest
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.options import Options as FirefoxOptions

class TestUntitled():
  def setup_method(self, method):
    options = FirefoxOptions() 
    options.add_argument('--headless') 
    self.driver = webdriver.Firefox(firefox_binary="/usr/bin/firefox-esr",options=options)
    # self.driver = webdriver.Chrome()
    self.vars = {}
  
  def teardown_method(self, method):
    self.driver.quit()
  
  def test_untitled(self):
    self.driver.get("https://luyingp.gitlab.io/-/destinycards-front/-/jobs/716028804/artifacts/public/staging/index.html")
    self.driver.set_window_size(1920, 923)
    self.driver.implicitly_wait(20)
    self.driver.find_element(By.CSS_SELECTOR, ".default_card:nth-child(1) > .front").click()
    assert self.driver.find_element(By.CSS_SELECTOR, ".fliped > .back").text == "Nature does not hurry, yet everything is accomplished. - Lao Tseu"
    self.driver.find_element(By.CSS_SELECTOR, ".default_card:nth-child(3) > .front").click()
    assert self.driver.find_element(By.CSS_SELECTOR, ".default_card:nth-child(3) > .back").text == "Those who have knowledge, don\'t predict. Those who predict, don\'t have knowledge. - Lao Tseu"
  
