FROM python:3.6

EXPOSE 5000

WORKDIR /python

COPY requirements.txt /python
RUN pip3 install -r requirements.txt

COPY app.py /python
CMD python app.py

